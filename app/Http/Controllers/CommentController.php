<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{


    public function store(Request $request)
    {
        $request->validate([

            'description' => 'required'
        ]);
        // to get any comment from user
        $input = $request->all();
        //to get user_id by auth
        $input['user_id'] = auth()->user()->id;
        Comment::create($input);
        return back();
    }
}
